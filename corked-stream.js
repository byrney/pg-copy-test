const { Writable } = require('stream')

class Corked extends Writable {
  constructor(text, options) {
    super(options)
    this.text = text
    this._gotCopyInResponse = false
    this.cork()
  }

  submit() {
      console.log('uncork');
      this.uncork();
  }

  _write(chunk, enc, cb) {
      console.log('_write', chunk);
      cb();
  }

  _writev(chunks, cb) {
      console.log('_writev', chunks.length);
      cb();
  }

}


function main(){
    const s = new Corked('hello');
    [1, 2, 3, 4].forEach(i => s.write(`line: ${i}`));
    setTimeout(() => {
        s.submit();
        [5, 6, 7, 8].forEach(i => s.write(`line: ${i}`));
    }, 1000);
}

main();

