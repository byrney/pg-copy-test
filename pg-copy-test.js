const stream = require('stream');
const pg = require('pg');
const pgp = require('pg-promise');
const _ = require('lodash');
const CopyFrom = require('./copy-from.js');
const childProcess = require('child_process');
const program = require('commander');

// http://adpgtech.blogspot.com/2014/09/importing-json-data.html
const DELIM=String.fromCharCode(1);  // characters that are not valid in json
const QUOTE=String.fromCharCode(2);
//return client.query(copyFrom(`copy ${namespace}.${tableName} from stdin csv delimiter e'\x01' quote e'\x02'`));

function createTable(conn, table, namespace){
    const sql = `
        create schema if not exists $(namespace:raw);
        drop table if exists $(namespace:raw).$(table:raw);
        create table $(namespace:raw).$(table:raw) (
            test_int int,
            test_string text
        );
    `;
    return conn.none(sql, {table, namespace});
}

function getRows(conn, table, namespace){
    const sql = `
        select count(*) from $(namespace:raw).$(table:raw);
    `;
    return conn.one(sql, {table, namespace});
}

function uploadCsvCopy(table, namespace, csv){
    const client = new pg.Client({});
    return client.connect()
        .then(() => pgCopyCsv(client, table, namespace, csv))
        .catch(console.error)
        .finally(() => client.end())
    ;
}

function pgCopyCsv(client, table, namespace, csv){
    return new Promise((res, rej) => {
        const toStream = new CopyFrom(`copy ${namespace}.${table} from stdin csv`, { maxBuffer: 1024 * 512 });
        console.time('stream time');
        client.query(toStream);
        csv.pipe(toStream);
        toStream.on('finish', () => {
            console.timeEnd('stream time');
            res();
        });
    });
}

function uploadCsvChild(table, namespace, csv){
    return new Promise((res, rej) => {
        const cp = childProcess.spawn('psql', ['-c', `\copy ${namespace}.${table} from stdin csv`], {shell: false});
        cp.on('close', () => {
            console.timeEnd('stream time');
            res();
        });
        console.time('stream time');
        csv.pipe(cp.stdin);
    });
}

function uploadCsvStdout(table, namespace, csv) {
    return new Promise((res, rej) => {
        stream.pipeline(csv, process.stdout, res);
    });
}

const sinks = {
    psql: uploadCsvChild,
    stdout: uploadCsvStdout,
    pgcopy: uploadCsvCopy
}

const csvSources = {
    preload: (count) => {
        const s = stream.Readable();
        _.times(count, n => {
            s.push(`${n},Hello ${n}\n`);
        });
        s.push(null);
        return s;
    },

    onestring: (count) => {
        const s = stream.Readable();
        s.push(_.times(count, n => `${n},Hello ${n}`).join('\n'));
        s.push(null);
        return s;
    },

    pull: (count) => {
        let n = 0;
        return new stream.Readable({
            read(size) {
                for(let i = 0; i < size; i++) {
                    if(n >= count) {
                        this.push(null);
                        break;
                    } else {
                        this.push(`${n},Hello ${n}\n`);
                        n += 1;
                    }
                }
            }
        });
    }
}

function main(cmdSource, cmdSink, cmdOptions) {
    const pool = pgp({schema: ['staging', 'public']})({application_name: 'test-pg-copy'});
    const namespace = 'temptest';
    const table = 'test_copy_streams';
    const rows = cmdOptions.rows
    const csv = csvSources[cmdSource](rows);
    const sink = sinks[cmdSink];
    console.log(`${cmdOptions.rows} rows of csv from ${cmdSource} to ${cmdSink}`);
    return pool.connect({direct: true})
        .then(conn => {
            return createTable(conn, table, namespace)
                .then(() => sink(table, namespace, csv))
                .then(() => getRows(conn, table, namespace))
                .then(res => console.log(`Table contains [${res.count}] rows`))
                .finally(() => {
                    conn.done();
                })
        })
        .catch(console.error)
        .finally(() => pool.$pool.end())
    ;
}

program
    .argument('[source]', 'source of csv files: preload, pull or onstring', 'preload')
    .argument('[sink]', 'Method of writing to psql: pgcopy, psql, stdout', 'pgcopy')
    .option('-r --rows <number>', 'number of rows to copy', 1e6)
    .action(main)
    .parse()
;

